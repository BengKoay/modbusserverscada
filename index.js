// create an empty modbus client
// const { add } = require('bignum');
let ModbusRTU = require('modbus-serial');
// const buffer = new ArrayBuffer(16);
let msg = [0, 16833];
let pay = msg;
let buf = Buffer.allocUnsafe(4);
buf = pay;
// buf.writeUInt16BE(pay[0], 2);
// buf.writeUInt16BE(pay[1], 0);
// const arr = new Float32Array([21, 31]);
// const view = new DataView(buffer);
// view.setFloat32(1, Math.PI);
const Float32ToHex = (float32) => {
	const getHex = (i) => ('00' + i.toString(16)).slice(-2);
	var view = new DataView(new ArrayBuffer(4));
	view.setFloat32(0, float32);
	return Array.apply(null, { length: 4 })
		.map((_, i) => getHex(view.getUint8(i)))
		.join('');
};

let solarVoltage = 235.6; // (voltage1 + voltage2 + voltage3 ) / 3 * 10     "voltageA": 235.6,
let solarCurrent = 100; // current1 + current2 + current3 * 10            "currentA": 44.5,
let solarActivePower = 100000; // active power * 1000                        "activePower": 72,
let solarReactivePower = 0.041 * 1000; // reactive power * 1000                    "reactivePower": 0.041,
let vector = {
	getHoldingRegister: function (addr, unitID) {
		// return addr + 8000;
		if (addr === 19000)
			return Number(`0x${Float32ToHex(solarActivePower).slice(0, 4)}`); // P
		if (addr === 19001)
			return Number(`0x${Float32ToHex(solarActivePower).slice(4, 8)}`); // P
		if (addr === 19002)
			return Number(`0x${Float32ToHex(solarReactivePower).slice(0, 4)}`); // Q
		if (addr === 19003)
			return Number(`0x${Float32ToHex(solarReactivePower).slice(4, 8)}`); // Q
		if (addr === 19004)
			return Number(`0x${Float32ToHex(solarCurrent).slice(0, 4)}`); // I
		if (addr === 19005)
			return Number(`0x${Float32ToHex(solarCurrent).slice(4, 8)}`); // I
		if (addr === 19006)
			return Number(`0x${Float32ToHex(solarVoltage).slice(0, 4)}`); // V
		if (addr === 19007)
			return Number(`0x${Float32ToHex(solarVoltage).slice(4, 8)}`); // V
		// if (addr === 1001) return buf[2]; // voltage * 10
		// if (addr === 1001) if (solarVoltage > 65535) return solarVoltage - 65535; // voltage * 10
		// if (addr === 1002) return solarVoltage;
		// if (addr === 1003) if (solarCurrent > 65535) return solarCurrent - 65535; // current * 10
		// if (addr === 1004) return solarCurrent;
		// if (addr === 1005)
		//   if (solarActivePower > 65535) return solarActivePower - 65535; // active power * 1000
		// if (addr === 1006) return solarActivePower;
		// if (addr === 1007)
		//   if (solarReactivePower > 65535) return solarReactivePower - 65535; // reactive power * 1000
		// if (addr === 1008) return solarReactivePower;
		// return addr + 8000;
	},
	readDeviceIdentification: function (addr) {
		return {
			0x00: 'PlusXnergyEdge',
		};
	},
};

// set the server to answer for modbus requests
console.log('ModbusTCP listening on modbus://127.0.0.1:502');
let serverTCP = new ModbusRTU.ServerTCP(vector, {
	host: '192.168.100.3',
	port: 502,
	debug: true,
	unitID: 1,
});

serverTCP.on('socketError', function (err) {
	// Handle socket error if needed, can be ignored
	console.error(err);
});

////////////////////////

// create an empty modbus client
// var ModbusRTU = require("modbus-serial");
// var vector = {
//   getInputRegister: function(addr, unitID) { return addr; },
//   getHoldingRegister: function(addr, unitID) { return addr + 8000; },
//   getCoil: function(addr, unitID) { return (addr % 2) === 0; },
//   setRegister: function(addr, value, unitID) { console.log('set register', addr, value, unitID); return; },
//   setCoil: function(addr, value, unitID) { console.log('set coil', addr, value, unitID); return; }
// };

// set the server to answer for modbus requests
// console.log('ModbusTCP listening on modbus://0.0.0.0:502');
// var serverTCP = new ModbusRTU.ServerTCP(vector, { host: '0.0.0.0' });
